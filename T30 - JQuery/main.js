var num1;
var num2;
var op;

$(document).ready(function () {
    $("#n1").on("click", function () {
        $("#pantalla").append("1");
    });
    $("#n2").on("click", function () {
        $("#pantalla").append("2");
    });
    $("#n3").on("click", function () {
        $("#pantalla").append("3");
    });
    $("#n4").on("click", function () {
        $("#pantalla").append("4");
    });
    $("#n5").on("click", function () {
        $("#pantalla").append("5");
    });
    $("#n6").on("click", function () {
        $("#pantalla").append("6");
    });
    $("#n7").on("click", function () {
        $("#pantalla").append("7");
    });
    $("#n8").on("click", function () {
        $("#pantalla").append("8");
    });
    $("#n9").on("click", function () {
        $("#pantalla").append("9");
    });
    $("#n0").on("click", function () {
        $("#pantalla").append("0");
    });

    //Operadores
    $("#c").on("click", function () {
        borrar();
    });

    $("#ce").on("click", function () {
        borrar();
    });

    $("#restar").on("click", function () {
        num1 = $("#pantalla").html();
        op = "-";
        limpiar();
    });


    $("#multiplicar").on("click", function () {
        num1 = $("#pantalla").html();
        op = "*";
        limpiar();
    });


    $("#dividir").on("click", function () {
        num1 = $("#pantalla").html();
        op = "/";
        limpiar();
    });

    $("#sumar").on("click", function () {
        num1 = $("#pantalla").html();
        op= "+";
        limpiar();
    });

    $("#igual").on("click", function () {
        num2 = $("#pantalla").html();
        operaciones();
    });

    $("#porcentage").on("click", function () {
        num1 = $("#pantalla").html();
        op = "%";
        limpiar();
    });

    $("#retr").on("click", function () {
        num1 = $("#pantalla").html();
    });

    $("#raiz").on("click", function () {
        num1 = $("#pantalla").html();
      
    });
});

//Operaciones y funciones

function limpiar() {
    $("#pantalla").html("");
}

function borrar() {
    $("#pantalla").html("");
    num1 = 0;
    num2 = 0;
    op = "";
}

function operaciones() {
    var res = 0;
    switch (op) {
        case "+":
            res = parseFloat(num1) + parseFloat(num2);
            break;

        case "-":
            res = parseFloat(num1) - parseFloat(num2);
            break;

        case "*":
            res = parseFloat(num1) * parseFloat(num2);
            break;

        case "/":
            res = parseFloat(num1) / parseFloat(num2);
            break;

        case "%":
            res = parseFloat((num1)*parseFloat(num2))/100;
            break;
    }
    borrar();
    $("#pantalla").html(res);
}